As outlined in the different chapters of this document, the interpretation of the results of the data analysis of an untargeted metabolomics experiment is a complex task. This is due to several factors such as the limited features that the existing software offer but also the complexity of the data. In chapter~\ref{c:pimp}, the issues in supporting the metabolomics users were partly addressed by providing some biological context to the results. Hence, pathways visualisation tools and filters around the biological context were developed to improve the users' experience and help them extracting meaningful information. Also, PiMP was developed using a modular design; the previous section made use of it to extend the features with the integration of an internal plugin to analyse fragmentation data. The work presented here makes use of this modular design to integrate an external plugin to extend the data interpretation capabilities of PiMP by adding biological network analysis support (aim number 4). 

% 4.5.1 Network analysis and visualisation of metabolomics data
\subsection{Network reconstruction}
\label{c:extendedpimp:network:analysis}

%\input{"40-extended_PiMP/45-network/451-analysis.tex"}/
A similar approach as the chemical library (section~\ref{c:extendedpimp:studydesign:biochemicallibrary}) was taken to develop this tool. Several steps are required to present the metabolic network of the system or organism studied to the user; the network also needs to be enriched with the metabolomics data present in PiMP. This multi-step task requires user interaction in order to select the appropriate model and sub-network to visualise; it was therefore developed as a semi-automated tool. Two types of information need therefore to be captured by the user, (i) the network model, (ii) the sub network parts divided in biological pathways. Figure~\ref{fig:pimp_network_form} shows the form presented to the user for this purpose. 

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/45-network/figures/network_form.png}
\par\end{centering}
\caption[Integrated network analysis form]{\label{fig:pimp_network_form} Dynamic Network analysis form allowing the user to select the organism and pathways to visualise.}
\end{figure}

MetExplore's metabolic networks database was chosen to reconstruct the network. The communication is, however, different than the protocol used for the chemical library as it requires user interaction. As PiMP provides InChIKey identifiers for all metabolites present in its database, the metabolite mapping onto MetExplore network was developed using these identifiers. Figure~\ref{fig:pimp_network_com} shows the communication protocol between PiMP and MetExplore based on REST web service. 

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/45-network/figures/network_reconstruction_com.png}
\par\end{centering}
\caption[PiMP - MetExplore communication]{\label{fig:pimp_network_com} Communication between PiMP and MetExplore web service to dynamically perform a metabolite mapping on the model selecting and build the network requested. \textbf{1}. Request from the user is sent to the view. \textbf{2} and \textbf{3}. The view request the list metabolites to map from the model layer. \textbf{4}. The list is sent to MetExplore webservice with the selected organism and pathway. \textbf{5}. The reconstructed network is sent back to PiMP. \textbf{6}. After enrichment of the network with intensity values, the network is sent back to the template for display purposes.}
\end{figure}

The first step is to provide the user with the list of different networks available, therefore, when the user requests a network analysis, PiMP sends a request to MetExplore web service to gather this information. The list of networks is returned to PiMP as a JSON array and presented to the user in a drop-down list (Figure~\ref{fig:pimp_network_form}). Once the user has selected the desired network, a new call is sent to MetExplore to retrieve information regarding the pathways available in this network. This second call to MetExplore web service is sent with the list of metabolites to map on the network, the data returned to PiMP is, therefore, a list of pathway present in the network with both the total number of metabolites present in the pathway and the number of metabolite mapped from the list sent by PiMP. The data sent and received by PiMP is also formatted as JSON arrays. This allows presenting to the user more accurate information to assist with the pathway selection as seen in Figure~\ref{fig:pimp_network_form}. Finally, once the selection of pathways has been performed, a``launch" button allows the user to start the network visualisation; however, a last call is required to reconstruct the network using the user selections. The request sent from PiMP to MetExplore contain the following information, (i) the BioSource id, (ii) the list of pathways, (iii) the list of InChIKey identifiers to map.
MetExplore create the network using this information and return it to PiMP as a JSON structure. PiMP then  add the intensity values for each mapped metabolite to the network by parsing the JSON structure. The resulting network contains all information necessary to be visualised and explored by the user; it is therefore sent to the PiMP template for visualisation purposes.

% 4.5.2 Integration of network analysis tool in metabolomics pipeline
\subsection{Network visualisation}
\label{c:extendedpimp:network:integration}

Several software and computing libraries provide user interfaces to visualise, explore and mine biological networks. However, the library used needs to meet specific requirements to be integrated into PiMP. As PiMP uses the web browser to present the data to the user, the network visualisation library needs, therefore, to use web technologies to be integrated into PiMP data exploration environment. MetExploreViz was chosen for this purpose, MetExploreViz is a network visualisation plugin that is used and developed as part of MetExplore. It has been developed using D3.js javascript library~\cite{Bostock2011} which is specifically designed to develop web-enabled data visualisation tool. MetExploreViz integrates network mining and comparison tools as well as most of the common features such as search and export features. As this library is specifically designed for biological network visualisation, it also includes bespoke features to support tasks that biologists may want to perform, visualising pathways, cell compartments or duplicating highly connected nodes to simplify the network. In metabolic networks, highly connected nodes, also known as side compounds, are compounds that take part in many reactions and have little biological meaning such as water or CO\textsuperscript{2}. If displayed as one single node, it can create a hairball effect and increase the complexity of the network by generating biologically irrelevant paths between the different compounds of the network, making it difficult to interpret (Figure~\ref{fig:network_side_compounds}). The last benefit of using this javascript library is the input format. Indeed, as the network is reconstructed using MetExplore web service, it is already in the right format and can directly be loaded in the visualisation plugin. This avoids unnecessary network transformation and therefore limits the generation of errors that can happen during the translation of the network to another format. 

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/45-network/figures/network_side_compounds.png}
\par\end{centering}
\caption[Network visualsisation and filter tools]{\label{fig:network_side_compounds} Visualisation of the same network before and after duplication of "side compounds". \textbf{A}. Original network. \textbf{B}. Same network after duplicating nodes considered as "side compounds" to allow a better interpretation.}
\end{figure}

Once the user has performed all the tasks necessary to reconstruct the network (as explain in the previous section), PiMP template receive the signal from the view layer to start MetExploreViz and load the network. The communication between PiMP and MetExploreViz plugin is directly happening within the template using javascript. Figure 4.21 shows how MetExploreViz network visualisation is integrated into a new tab of PiMP data exploration environment. The user can perform new network reconstruction using the same interface as used initially (Figure~\ref{fig:pimp_network_form}) to load a new network. 
%\input{"40-extended_PiMP/45-network/452-integration.tex"}

\begin{sidewaysfigure}
%\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/45-network/figures/metexploreviz_integration.png}
\par\end{centering}
\label{fig:network_tab_int}
\caption[PiMP integrated network visualisation]{Network visualisation as seen in PiMP data exploration environment. A new tab called "Network" is created on the fly, this new tab embed MetExploreViz javascript plugin and communicate with PiMP to display the reconstructed network.}
%\end{figure}
\end{sidewaysfigure}

\clearpage