\epigraph{''Fragmentation is when you make big bits of stuff into teeny tiny wee bits.''}{\textit{Erin D. T. Manson}}

Metabolite identification remains the major challenge in metabolomics. Although external standards increase the degree of confidence in the identification of compounds, LCMS based metabolomics provides evidence to support metabolite identification but rarely with absolute certitude. This level of uncertainty is even more applicable to metabolites absent from the external standard compounds and therefore only annotated from the mass. Whilst in some cases putative annotation can be informative enough to guide the biological interpretation of results and extract meaningful insight from a dataset, it often leads to further investigation to ascertain the identity of a compound that might play an important role in the system studied. It is possible to routinely run liquid chromatography tandem mass spectrometry on mass spectrometers~\cite{Vogeser2007} (LCMS/MS). MS/MS acquisition produces product ion spectra (Figure~\ref{fig:frag_tree}) from which compounds' structural information can be derived. Coupling LCMS to MS/MS acquisition can, therefore, improve the level of confidence with which the compounds are identified and lead to a better interpretation of the results. As shown in Figure~\ref{fig:metabolomics_workflow_fragmentation}, the identification step is upstream of the biological interpretation in the data analysis pipeline, the quality of compound identification has therefore a critical impact on the biological interpretation.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=0.80\textwidth]{40-extended_PiMP/figures/frag_tree.png}
\par\end{centering}
\caption[Tandem MS spectra]{\label{fig:frag_tree} Representation of fragmentation spectra produced by tandem MS.}
\end{figure}

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/figures/metabolomics_workflow_fragmentation.png}
\par\end{centering}
\caption[Metabolomics workflow improvement area using fragmention data]{\label{fig:metabolomics_workflow_fragmentation} Fragmentation helps improving the identification part of the metabolomics workflow and ultimately improves the biological interpretation of the results.}
\end{figure}

Two type of acquisition can be used to gather MS/MS information, (i) Data-Dependent acquisition (DDA) which is supervised and only fragment precursor ions above a predefined abundance threshold, (ii) Data-Independent acquisition (DIA), however, fragments all ions within a certain m/z window without selection between compounds eluting at the same retention time. However, fragmentation spectra produced by DIA are more complex to analyse as no precursor ion selection is performed~\cite{Doerr2014}. 

The approach taken here attempt to support DDA fragmentation data analysis as part of the untargeted metabolomics workflow to improve metabolite identification. The tool developed was designed as a flexible internal PiMP plugin to be used as part of the data analysis pipeline in place or as a separate analysis tool. The Fragmentation Annotation Kit (\ac{frank}) was implemented by Scott J. Greig, Karen McLuskey and Joe Wandy, the role of the author was to coordinate the development and the integration of the tool within PiMP.

As FrAnK is developed for identification purposes, the minimum requirement for the tool to run is the upload of the files corresponding to the pool samples which contain fragmentation data. Indeed, as the pool samples contain every compound present in all experimental samples of a study, it is the only input data required to perform the peak annotation.

\subsection{Annotation tool and library}
\label{c:extendedpimp:fragmentation:annotation}

Several tandem mass spectra libraries and tools to search them and compare experimental mass spectra to known and annotated fragmentation spectra exist. However, many of them are not adequate for the development of FrAnK. Indeed, some libraries are only accessible through a web browser, others offer a web service alternative but do not allow the submission of several peaks at the same time. Finally, although some tools are programmatically accessible, they do not permit local installation and force the database search to be performed through the web which increases the processing time greatly. The tool chosen to overcome these limitations is MS PepSearch, which was associated with the MassBank~\cite{Horai2010} and the US National Institute of Science and Technology (\ac{nist}) library. The tool and libraries were deployed in a Docker~\cite{Merkel2014} container so they can be installed and used easily on any operating system.

\subsection{FrAnK architecture and design}
\label{c:extendedpimp:fragmentation:design}

As FrAnK was developed as an internal plugin of PiMP, the same design approach was taken. FrAnK is, therefore, a complex Django app that extends the core functionalities of PiMP. As explained in Chapter~\ref{c:pimp}, PiMP is built using three main modules supporting data capture, data analysis and result exploration, each modules being divided into 'apps'. As FrAnK needs to support fragmentation data analysis both independently and as part of the PiMP pipeline, it was designed as a single app supporting data capture, analysis and result exploration. The FrAnK app, therefore, defines its own data models, its own views and templates. 

The data processing happens asynchronously using the same task system as PiMP. A python wrapper was created around the annotation tools to allow communication between the task layer and the data analysis pipeline. This means that no exchange format is required to transfer the data output of the annotation tools back to the task and store them permanently in the database.

\subsection{Data capture and visualisation}
\label{c:extendedpimp:fragmentation:integration}

Data capture visualisation support was developed so the tool can be used independently from PiMP. The data capture follows the same design as PiMP asking the user to perform a sequence of tasks from file upload to starting the annotation pipeline. Metadata can also be entered to document the experiment and analysis performed. 

The results of FrAnK annotation pipeline can be explored in a dedicated data exploration environment and consist of two main pages. The first page display a table containing the list of MS\textsuperscript{1} peaks detected with associated information such as the mass, retention time and intensity. Each peak can then be explored further by clicking on the identifier, giving access to the "single peak" page displaying the fragmentation spectra and the different putative annotations respectively shown in Figure~\ref{fig:FrAnK_spectra} and Figure~\ref{fig:FrAnK_annot_page}. Extra information is given such as the structure of the compound annotated by the peak and confidence score returned by the annotation tools.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=0.80\textwidth]{40-extended_PiMP/figures/FrAnK_spectra.png}
\par\end{centering}
\caption[FrAnK fragment spectrum visualisation]{\label{fig:FrAnK_spectra} Fragment spectrum as seen in FrAnK dedicated data visualisation interface. Courtesy of Karen McLuskey, personal communication.}
\end{figure}

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/figures/FrAnK_annot_page.png}
\par\end{centering}
\caption[FrAnK annotation page]{\label{fig:FrAnK_annot_page} FrAnK dedicated annotation page. Courtesy of Karen McLuskey, personal communication.}
\end{figure}

\subsection{PiMP-FrAnK integration}
\label{c:extendedpimp:fragmentation:integration}

In order for the user to be able to use FrAnK seamlessly within PiMP, the two tools had to be integrated at different levels. However, as PiMP offers a modular design, only very specific parts of the source code had to be modified with no impact on the rest of the tool. Three main points of communication had to be created for the tools to run in concert. First, a connection between the two different data structures is required to give PiMP access to the fragmentation data. Then, PiMP ``data capture'' modules need to encapsulate FrAnK data capture to unify the data capture as a single task, and finally, PiMP data interpretation module has to present the fragmentation results to the user.

\subsubsection{Data structure connection}
\label{c:extendedpimp:fragmentation:integration:datastructure}

FrAnK was developed as an integral part of PiMP but also a fully independent Django app. The same technologies were used across the two tools. They, therefore, share a MySQL database, both tools defining its own tables with the exception of the user table which is shared between the two systems. For the tools to communicate and share data, two joining database tables had to be created to form a cohesive data structure. The first joining table connects an experiment in FrAnK to a PiMP project as shown in Figure~\ref{fig:pimp_frank_project_connection}; this high-level join allows PiMP to access FrAnK data tables corresponding to the data capture.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/figures/pimp_frank_project_connection.png}
\par\end{centering}
\caption[PiMP - FrAnK database join at the project level]{\label{fig:pimp_frank_project_connection} Connection between PiMP and FrAnK at the project level of the data structure. The user table is shared between the two tools.}
\end{figure}

The second joining table was created to unify the results of the two pipelines at the peak level; this joining table, therefore, connects MS\textsuperscript{1} peaks from FrAnK to peaks in PiMP (Figure~\ref{fig:pimp_frank_peak_connection}). This join creates a "one to one" relationship between MS\textsuperscript{1} peaks found in the two systems. This connection gives then an extra layer of information for every peak extracted by the PiMP analysis pipeline which has now access to the fragmentation annotations generated by FrAnK.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/figures/pimp_frank_peak_connection.png}
\par\end{centering}
\caption[PiMP - FrAnK database join at the peak level]{\label{fig:pimp_frank_peak_connection} Connection between PiMP and FrAnK at the peak level of the data structure. This connection allows to bring together the results of the two data analysis pipelines.}
\end{figure}

\subsubsection{Integrated data analysis pipeline}
\label{c:extendedpimp:fragmentation:integration:analysispipeline}

Whilst LCMS/MS gives in-depth information on the structure of the compounds analysed, the acquisition of several isolated MS/MS spectra for each MS spectrum typically requires a longer duty cycle than for MS alone. This results in a lower number of mass scans acquired per run for the MS\textsuperscript{1} as seen in Figure~\ref{fig:MS_vs_MSMS}. As peak detection algorithms' performances are closely related to the peak shapes and therefore the number of time points available to assess if a signal corresponds to a peak or simple noise, the reduction in datapoints reduces the quality of peak detection. Peak detection can be used independently in FrAnK. The Peak detection in PiMP is more robust, applied to MS\textsuperscript{1} data only. This can, however, be used as an advantage by using the list of peaks detected in PiMP to feed the fragmentation data analysis. Consequently, when FrAnK is run as part of the PiMP pipeline, no peak detection step is required. This implies that the two data analysis pipeline cannot be run simultaneously as FrAnK requires an input from PiMP data analysis pipeline. As shown in Figure~\ref{fig:chain_pipeline}, the asynchronous task system used to run the data analysis pipelines offers the possibility to chain tasks. The fragmentation data analysis pipeline, when running as part of PiMP, only starts when the untargeted metabolomics data analysis pipeline is complete. The implementation of the modular pipeline architecture is detailed in section~\ref{c:pimp:untargetedpipeline:analsysistools} of chapter~\ref{c:pimp}. The connection between the peak entries created by both tools is performed as an extra trivial step during the storage of the results in the database.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=0.70\textwidth]{40-extended_PiMP/figures/MS_vs_MSMS.png}
\par\end{centering}
\caption[Comparison of MS\textsuperscript{1} peak between MS and MS/MS acquisition]{\label{fig:MS_vs_MSMS} Schematic of a MS\textsuperscript{1} chromatographic peak comparing MS and MSMS acquisition. A higher number of number of MS\textsuperscript{1} mass scans in single MS allows better resolution.}
\end{figure}

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=0.80\textwidth]{40-extended_PiMP/figures/chain_pipeline.png}
\par\end{centering}
\caption[PiMP chained pipeline]{\label{fig:chain_pipeline} Representation of chained pipelines, FrAnK using the results of PiMP as an input, this input being the list of detected peaks.}
\end{figure}

\subsubsection{Integrated data visualisation}
\label{c:extendedpimp:fragmentation:integration:results}

That last integration step happens in the template layer in order to let users start a fragmentation data analysis as part of PiMP. PiMP data capture template was therefore extended to allow the upload of fragmentation files. When PiMP detects the presence of fragmentation file, it allows the user to choose to run FrAnK annotation pipeline at the time of starting the analysis using a simple tick box. The upload of fragmentation files is optional to the user who can choose to run the PiMP data analysis pipeline on its own.

The results of the fragmentation data analysis were also integrated into PiMP data exploration environment. The first point of integration is located in the peak tab where an extra column is added to the table to display fragmentation information. When a peak entry is selected, the right panel gives direct access through a link to FrAnK dedicated result page for the selected peak (Figure~\ref{fig:FrAnK_peak_tab}).

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/figures/FrAnK_peak_tab.png}
\par\end{centering}
\caption[Fragmentation data in PiMP results environment]{\label{fig:FrAnK_peak_tab} Integration of FrAnK information in the peak tab of PiMP data exploration environment. An extra column shows if a fragmentation data annotation exist for every peak. The right panel also shows this information and allow access to FrAnK result page. Courtesy of Karen McLuskey, personal communication.}
\end{figure}

The fragmentation information was also integrated into the metabolite tab of PiMP data exploration environment. As this tab is central to the interpretation of the results, including fragmentation information in this part of the user interface can greatly help the user in assessing the quality of peak annotations. The fragmentation information is embedded in the contextual right panel of the metabolite tab by extending the peak card. As shown in Figure~\ref{fig:peak_card_frag}, when a peak has fragmentation information attached to it, an extra line is added to the peak card giving the name of the compound with the highest score provided by the fragmentation analysis. An extra button at the bottom of the card also gives access to FrAnK dedicated result page (opened in a blank page of the browser) to explore the MS\textsuperscript{n} spectra and the different possible annotations.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=0.60\textwidth]{40-extended_PiMP/figures/peak_card_frag.png}
\par\end{centering}
\caption[Fragmentation data in peak card]{\label{fig:peak_card_frag} Peak card from the metabolite tab of PiMP data exploration environment showing fragmentation data. FrAnK best annotation is given and an extra button (in green) gives access the to FrAnK dedicated peak page.}
\end{figure}
