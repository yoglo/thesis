Untargeted metabolomics aims to provide a snapshot of the metabolism state of a biological system at a specific time. It is therefore not possible to predict accurately the pool of chemical compounds that will be detected. However, the method chosen and the system studied can be used to provide information on chemicals that can potentially be detected and therefore help biologists designing their experiments. The chemical library presented in this section aims to help biologists understand what part of the metabolism of the system studied can be seen using untargeted metabolomics. The work from this section has been published in \textit{Frontiers in Molecular Biosciences}~\cite{Merlet2016} and tries to inform biologists about the potential outcome of an untargeted metabolomics experiment according to the organism studied (aim number 3). The role of the author was to develop the user interface and controllers on the PiMP side, the communication protocol between the two servers in collaboration with MetExplore developers. 

The approach taken here is to use the list of standard compounds run at Glasgow Polyomics routinely for metabolite identification and map them onto genome-scale reconstruction of metabolic networks available in MetExplore~\cite{Cottret2010}. The information returned by this type of approach would allow the user to know the coverage of the standard compounds on a particular metabolic network or organism. PiMP was used to communicate with MetExplore and display the results to the user.

\subsubsection{Metabolite identifiers}
\label{c:extendedpimp:studydesign:biochemicallibrary:metid}

In order to map metabolites present in Glasgow Polyomics standard compound library onto MetExplore's metabolic networks, the same identifiers need to be used by both tools. Many database specific identifiers can be utilised for metabolites such as KEGG~\cite{Kanehisa2014}, ChEBI~\cite{Hastings2013} and PubChem~\cite{Kim2016} identifiers. However, those identifiers cannot be used for this type of mapping as they are not commonly used to reference metabolites in metabolic networks, and some metabolites found in metabolic networks are not referenced in any of those databases. While the metabolomics community is currently putting effort in standardising the identification of metabolites using specific identifiers and controlled vocabulary~\cite{Salek2015}, alternative identifiers based on chemical structures can be used to overcome the issues met with the database specific identifiers. MetExplore uses the InChI (IUPAC International Chemical Identifier) and InChIKey identifiers to reference the metabolites in metabolic networks. The InChI identifiers provide a non-ambiguous identification of compounds organised in layers that provide different information about the structure of the molecule. The InChIKey is a hashed version of the InChI forming a 27 uppercase characters identifier. The InchIKey can be calculated from the InChI using a hash algorithm and is the identifier that was selected to map metabolites from the Glasgow Polyomics chemical library to MetExplore's metabolic networks.

\subsubsection{Metabolite mapping and communication protocol}
\label{c:extendedpimp:studydesign:biochemicallibrary:metmapcom}

Communication between the network database and the chemical library is necessary to map the metabolites onto the biological networks. The protocol proposed for this communication is based  on a dialogue between web services located on the two servers. As illustrated in Figure~\ref{fig:pimp_metex_com}, a four steps dialogue process has been created to perform the mapping and receive the results back on the chemical library server to be then presented to the user. The communication process is initiated by the chemical library server (PiMP) informing the network web server that a mapping is requested and providing some specific information. The information provided is the location (URL of chemical library web service) of the list of metabolite to map. As discussed in the previous section, the identifiers used for the metabolites are InChIKey identifiers formatted as a JSON array. Once the call is received by the network web service, the URL passed by the chemical library is used to retrieve the list of metabolites to map by calling the chemical library web service. This second call goes therefore from the network server to the chemical library server. The chemical library server simply returns the list of InChIKey identifiers that need to be mapped as a JSON array. The network server then performs the mapping and returns the results to the chemical library server as a response to its very first call. 

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/43-study_design/figures/pimp_metexplore_communication.png}
\par\end{centering}
\caption[Chemical library and network database communication protocol]{\label{fig:pimp_metex_com} 4 steps communication protocol between the chemical library and network database. \textbf{1}. PiMP contact the newtork library to request a mapping. \textbf{2} and \textbf{3}. MetExplore gather the list of InChI using the chemical library webservice. \textbf{4}. The result of the mapping is sent back to the chemical library.}
\end{figure}

\subsubsection{Metabolite mapping response}
\label{c:extendedpimp:studydesign:biochemicallibrary:metmapresponse}

The results are formatted according to the JSON encoding, divided into sections corresponding to individual BioSource (MetExplore's biological network). Each BioSource section contains general information related to the BioSource itself, its name and organism strain, the source (KEGG, BioCyc, SBML), the version number and MetExplore identifier. The section also contains information related to the network, the total number of metabolites in the network, the total number of metabolites which have an identifier (InChIs or InChIKeys), the total number of unique identifiers present in the network. Finally, each BioSource section contains information related to the mapping, the total number of identifiers from the network mapped in the chemical library and the total number of unique identifiers from the network mapped in the chemical library. Those two numbers may be different from the total number of metabolites in a network as it is based on the compartments (cellular compartment), this means that if a metabolite is present in n compartments it will then be counted x times. If a metabolite is present in different parts of the network within the same compartment, it is considered as one. The mapping information also contains the library and network coverage, respectively the relative number of library identifiers mapped on the network and the relative number of metabolites from the network present in the library. It also contains the percentage of identifiers found in both the library and the network compare to the number of unique identifiers in the network. The last information is a MetExplore mapping id, which allows the user to access the mapping results directly in MetExplore user interface.

\subsubsection{Metabolite mapping results}
\label{c:extendedpimp:studydesign:biochemicallibrary:metmapresult}

This tool was developed within the context of Glasgow Polyomics (GP) metabolomics platform services; the GP compounds library contains a list of 240 metabolites that are routinely run as standard compounds for identification purposes during the data analysis. GP is involved in a wide range of research areas; it is therefore important for GP users to have access to the coverage information of a maximum number of organisms. Thus, the mapping of GP chemical library is performed on all networks available in MetExplore database. The results of the mapping returned by MetExplore web service is presented as table accessible through PiMP web interface. The table is automatically generated by PiMP Django backend which parses the JSON formatted results and converts it in a Javascript enriched HTML file. The table (presented in Figure~\ref{fig:gp_chemical_library}) currently contains almost 60 different metabolic networks with the mapping information attached to it, Javascript functions make the table interactive allowing the user to search, sort and filter it. The name of each network is also clickable to allow the visualisation in a new web browser window of a selected mapping in MetExplore. Figure~\ref{fig:metex_metabolite_tab} and~\ref{fig:metex_pathway_tab} respectively show the metabolite mapping and pathway coverage information as seen in MetExplore. Finally, the tools available in MetExplore allow the user to visualise the entire network or a selected subnetwork as shown in Figure~\ref{fig:metex_network_viz}.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/43-study_design/figures/chemical_library.png}
\par\end{centering}
\caption[Glasgow Polyomics standard library table]{\label{fig:gp_chemical_library} Glasgow Polyomics standard library table showing the coverage of the first 8 metabolic networks (alphabetically sorted).}
\end{figure}

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/43-study_design/figures/metEx_metabolite_tab.png}
\par\end{centering}
\caption[MetExplore metabolite table]{\label{fig:metex_metabolite_tab} Metabolite table as seen in MetExplore showing the compounds mapped in an extra column.}
\end{figure}

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/43-study_design/figures/metEx_pathway_tab.png}
\par\end{centering}
\caption[MetExplore pathway table]{\label{fig:metex_pathway_tab} Pathway table as seen in MetExplore displaying information related to the mapping.}
\end{figure}

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{40-extended_PiMP/43-study_design/figures/metEx_network.png}
\par\end{centering}
\caption[MetExplore visualisation of a reconstructed network]{\label{fig:metex_network_viz} Visualisation of the network resulting from the mapping within MetExplore. }
\end{figure}

\clearpage