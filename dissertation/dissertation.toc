\contentsline {chapter}{\numberline {1}Introduction}{13}{chapter.1}
\contentsline {section}{\numberline {1.1}Omics technologies}{13}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Omics layers}{13}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Omics interactions}{15}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Metabolomics}{15}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Mass spectrometry metabolomics workflow}{16}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}LCMS Metabolomics}{17}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Measurement and separation technologies}{17}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Data\ format}{18}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Data processing}{21}{subsection.1.3.3}
\contentsline {subsubsection}{Peak detection}{21}{section*.11}
\contentsline {subsubsection}{Peak alignment}{22}{section*.12}
\contentsline {subsubsection}{Data filtering}{23}{section*.13}
\contentsline {subsubsection}{Gap filling}{23}{section*.14}
\contentsline {subsubsection}{Peak grouping}{23}{section*.15}
\contentsline {subsubsection}{Peak identification}{24}{section*.16}
\contentsline {subsubsection}{Statistical analysis}{25}{section*.17}
\contentsline {subsection}{\numberline {1.3.4}Data analysis platforms}{25}{subsection.1.3.4}
\contentsline {subsubsection}{Shortcoming of current data analysis platforms}{27}{section*.20}
\contentsline {section}{\numberline {1.4}Programming languages, libraries and frameworks}{28}{section.1.4}
\contentsline {section}{\numberline {1.5}Biological networks}{29}{section.1.5}
\contentsline {section}{\numberline {1.6}Related work}{31}{section.1.6}
\contentsline {chapter}{\numberline {2}Materials and methods}{32}{chapter.2}
\contentsline {section}{\numberline {2.1}Software engineering}{32}{section.2.1}
\contentsline {section}{\numberline {2.2}Data format}{33}{section.2.2}
\contentsline {section}{\numberline {2.3}Data analysis pipeline}{33}{section.2.3}
\contentsline {section}{\numberline {2.4}Web framework}{34}{section.2.4}
\contentsline {section}{\numberline {2.5}Data visualisation}{34}{section.2.5}
\contentsline {chapter}{\numberline {3}A semi-automated pipeline for untargeted metabolomics}{35}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduction}{35}{section.3.1}
\contentsline {section}{\numberline {3.2}Related work}{36}{section.3.2}
\contentsline {section}{\numberline {3.3}Integrated metabolomics workflow}{38}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Data\ analysis\ workflow}{38}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Untargeted metabolomics pipeline}{39}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Data\ structure}{39}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Context-sensitive\ visualisation}{46}{subsection.3.4.2}
\contentsline {subsubsection}{Raw data visualisation}{46}{section*.33}
\contentsline {subsubsection}{Peak set visualisation}{49}{section*.41}
\contentsline {subsubsection}{Peak set interpretation}{50}{section*.43}
\contentsline {subsubsection}{Pathway annotation}{51}{section*.45}
\contentsline {subsection}{\numberline {3.4.3}Module\ based\ pipeline}{52}{subsection.3.4.3}
\contentsline {subsubsection}{Data capture}{54}{section*.47}
\contentsline {subsubsection}{Data processing}{55}{section*.49}
\contentsline {subsubsection}{Data interpretation}{55}{section*.50}
\contentsline {subsection}{\numberline {3.4.4}Data\ analysis}{55}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Data\ exchange\ and\ data\ sharing}{57}{subsection.3.4.5}
\contentsline {subsubsection}{Data exchange}{57}{section*.52}
\contentsline {subsubsection}{Data sharing}{57}{section*.53}
\contentsline {subsection}{\numberline {3.4.6}Data\ interpretation}{58}{subsection.3.4.6}
\contentsline {subsubsection}{Summary page}{59}{section*.54}
\contentsline {subsubsection}{Raw data}{60}{section*.57}
\contentsline {subsubsection}{Statistical analysis}{60}{section*.59}
\contentsline {subsubsection}{Biological pathways}{61}{section*.61}
\contentsline {subsubsection}{Metabolites}{62}{section*.63}
\contentsline {section}{\numberline {3.5}Discussion}{67}{section.3.5}
\contentsline {section}{\numberline {3.6}Conclusion}{70}{section.3.6}
\contentsline {chapter}{\numberline {4}Extended metabolomics workflow for biological sciences}{71}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{71}{section.4.1}
\contentsline {section}{\numberline {4.2}Related work}{72}{section.4.2}
\contentsline {section}{\numberline {4.3}Supporting study documentation}{74}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Project management system}{75}{subsection.4.3.1}
\contentsline {subsubsection}{Data structure}{75}{section*.70}
\contentsline {subsubsection}{Web-enabled tool}{77}{section*.72}
\contentsline {subsection}{\numberline {4.3.2}Biochemical library}{79}{subsection.4.3.2}
\contentsline {subsubsection}{Metabolite identifiers}{79}{section*.74}
\contentsline {subsubsection}{Metabolite mapping and communication protocol}{80}{section*.75}
\contentsline {subsubsection}{Metabolite mapping response}{80}{section*.77}
\contentsline {subsubsection}{Metabolite mapping results}{81}{section*.78}
\contentsline {section}{\numberline {4.4}Fragmentation data analysis}{84}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Annotation tool and library}{85}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}FrAnK architecture and design}{86}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Data capture and visualisation}{86}{subsection.4.4.3}
\contentsline {subsection}{\numberline {4.4.4}PiMP-FrAnK integration}{86}{subsection.4.4.4}
\contentsline {subsubsection}{Data structure connection}{87}{section*.87}
\contentsline {subsubsection}{Integrated data analysis pipeline}{89}{section*.90}
\contentsline {subsubsection}{Integrated data visualisation}{89}{section*.93}
\contentsline {section}{\numberline {4.5}Biological network analysis}{91}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Network reconstruction}{92}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Network visualisation}{93}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Discussion}{96}{section.4.6}
\contentsline {section}{\numberline {4.7}Conclusion}{99}{section.4.7}
\contentsline {chapter}{\numberline {5}Integrative analysis of omics datasets using a network approach}{100}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{100}{section.5.1}
\contentsline {section}{\numberline {5.2}Related work}{101}{section.5.2}
\contentsline {section}{\numberline {5.3}Study design}{102}{section.5.3}
\contentsline {section}{\numberline {5.4}Data acquisition}{103}{section.5.4}
\contentsline {section}{\numberline {5.5}Metabolomics data analysis}{105}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Quality control}{105}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Time course analysis}{109}{subsection.5.5.2}
\contentsline {subsubsection}{Control class}{109}{section*.105}
\contentsline {subsubsection}{PHA activated class }{110}{section*.107}
\contentsline {subsubsection}{Time course comparison}{110}{section*.109}
\contentsline {subsection}{\numberline {5.5.3}Biological class analysis}{110}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}Standard compounds analysis}{112}{subsection.5.5.4}
\contentsline {section}{\numberline {5.6}RNA-seq data analysis}{112}{section.5.6}
\contentsline {subsection}{\numberline {5.6.1}Data acquisition and analysis pipeline}{113}{subsection.5.6.1}
\contentsline {subsection}{\numberline {5.6.2}Gene networks}{114}{subsection.5.6.2}
\contentsline {section}{\numberline {5.7}Integrative analysis}{116}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}Multi omics network reconstruction}{116}{subsection.5.7.1}
\contentsline {subsubsection}{Gene mapping}{116}{section*.116}
\contentsline {subsubsection}{Metabolite mapping}{118}{section*.118}
\contentsline {section}{\numberline {5.8}Discussion}{119}{section.5.8}
\contentsline {section}{\numberline {5.9}Conclusion}{123}{section.5.9}
\contentsline {chapter}{\numberline {6}General discussion}{124}{chapter.6}
\contentsline {chapter}{\numberline {A}PiMP libraries}{129}{appendix.A}
\contentsline {section}{\numberline {A.1}R libraries}{129}{section.A.1}
\contentsline {section}{\numberline {A.2}Python libraries}{131}{section.A.2}
\contentsline {section}{\numberline {A.3}JavaScript libraries}{132}{section.A.3}
\contentsline {chapter}{\numberline {B}List of Standard compounds}{133}{appendix.B}
\contentsline {chapter}{\numberline {C}Integrated network pathway list}{138}{appendix.C}
\contentsline {chapter}{\numberline {D}List of metabolites mapped to the metabolic network}{140}{appendix.D}
\contentsline {chapter}{Bibliography}{142}{table.caption.128}
