The raw data contains the unprocessed signal measured by the instrument (mzXML, mzML or mzData). Raw data visualisation and curation tools are well supported by proprietary software such as Xcalibur (Thermo Fisher) that allows the user to drive the mass spectrometer itself. They are usually designed to support analytical chemists  to run samples through the instrument and assess the quality of the data acquired. In the context of PiMP, raw data visualisation is essential at the pre-processing stage (Figure~\ref{fig:raw-data-viz}) to quality control the data and allow internal standard checks.
To support these tasks, several visualisation tools are required.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=0.5\textwidth]{30-PiMP/34-untargeted_pipeline/figures/raw_data_vis.png}
\par\end{centering}
\caption{\label{fig:raw-data-viz} Raw data visualisation support within the metabolomics workflow.}
\end{figure} 

The first module supports the visualisation of the total ion current (TIC) of individual samples to assess the quality of the signal (Figure~\ref{fig:sample-tic}).

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{30-PiMP/34-untargeted_pipeline/figures/sample_TIC.png}
\par\end{centering}
\caption[Total Ion Current visualisation]{\label{fig:sample-tic} Total ion current of a single sample as viewed in PiMP.}
\end{figure} 

This basic representation of the raw data is particularly useful to visualise the overall signal present in the file as well as the background noise. The evaluation of individual mass scans (Figure~\ref{fig:mass-scan}) allows the identification of potential contaminants by locating specific signals throughout the run. 

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{30-PiMP/34-untargeted_pipeline/figures/mass_scan.png}
\par\end{centering}
\caption[Mass spectra visualisation]{\label{fig:mass-scan} Mass spectra of the sample displayed in Figure~\ref{fig:sample-tic} at retention time 2064.05 seconds .}
\end{figure} 

As metabolomics studies require biological replicates to be run to be statistically relevant and interpretable, the reproducibility of these replicates is a critical part the quality control stage. Three types of charts are usually used for the user to achieve this task easily. A stacked line plot of the TIC of each replicates (Figure~\ref{fig:group-tic} a) allows the identification of potential signal exclusive to one replicate which can be the signature of a contaminant. This visualisation also allows the user to easily identify potential time drift that might have appeared throughout the run of the samples during data acquisition, in which case a realignment of the signal would be necessary during data processing (Figure~\ref{fig:group-tic} b).

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=0.75\textwidth]{30-PiMP/34-untargeted_pipeline/figures/group_TIC.png}
\par\end{centering}
\caption[Total Ion Current of the positive ionisation of biological replicates]{\label{fig:group-tic} Total Ion Current of the positive ionisation of biological replicates as seen in PiMP. \textbf{a}. Replicate samples show high reproducibility, no time drift or contaminants can be identified. \textbf{b}. Replicate samples show good signal reproducibility but a time drift is clearly visible between the three replicates. Retention time correction will therefore be required during the analysis.}
\end{figure}

Two alternative plots of the mean and median TICs are also presented to the user and allow the assessment of the reproducibility of the overall signal of the replicates (Figure~\ref{fig:mean-median-tic}).

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=0.75\textwidth]{30-PiMP/34-untargeted_pipeline/figures/mean_median_tic.png}
\par\end{centering}
\caption[Mean and median Total Ion Current]{\label{fig:mean-median-tic} \textbf{a}. Mean Total Ion Current of the replicate samples of condition 2. \textbf{b}. Median Total Ion Current of the replicate samples of condition 2.}
\end{figure}

The search for known peaks of internal standards or other compounds in the samples is also an important task that the user may want to perform before proceeding to a more global analysis of the dataset. The control of the presence of specific features in the samples can have several purposes such as assessing the presence of essential compounds for the study or the reproducibility of an internal standard. A simple tool presented in Figure~\ref{fig:peak_discovery_tool} was created allowing the user to browse through the raw data by defining the following parameters: the mass, the retention time, the retention time and mass windows, the ionisation mode and the samples in which the search must be performed.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.00\textwidth]{30-PiMP/34-untargeted_pipeline/figures/peak_discovery_tool.png}
\par\end{centering}
\caption[Peak discovery tool]{\label{fig:peak_discovery_tool} Peak discovery tool interface allowing the user to search for specific features. In this example, the user is performing a search for the signal corresponding to the glucose compound or its isomers.}
\end{figure}

Once the search has finished processing, the signal found in every sample is presented within the same window as shown in Figure~\ref{fig:peak_discovery_results}, allowing the user to assess the peak shape or identifying samples with missing signals. Users can refine their search by clicking the New screening button and changing the parameters. The tool also allows the download of each figure as a picture for presentation purposes.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.00\textwidth]{30-PiMP/34-untargeted_pipeline/figures/peak_discovery_results.png}
\par\end{centering}
\caption[Peak discovery tool - results]{\label{fig:peak_discovery_results} Peak discovery tool result interface displaying the extracted ion current for each individual sample.}
\end{figure}
