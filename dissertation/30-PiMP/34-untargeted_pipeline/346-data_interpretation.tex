The interpretation of metabolomics data is a complex task that can require knowledge in different domains such as mass spectrometry, analytical chemistry, biochemistry or metabolism. It also often requires expertise in the specific question that the experiment is trying to address such as a broad knowledge of the organism, tissue or disease studied. However, guiding the user in the interpretation of the data resulting from a metabolomics experiment can help overcome the lack of knowledge in those fields. The work described is this section present the data exploration environment that was developed within the PiMP software to assist the user in the interpretation of its results; it, therefore, addresses the aim number 4 of this project. The data exploration environment makes use of the modular model of PiMP and more specifically of the context-sensitive visualisation modules discussed in chapter 3.4.2 to present the data to the user in a coherent and intuitive manner. 

The data exploration environment is presented as one unified page to the user and structured into tabs, each tab being laid out as a table with the exception of the summary page which provides an overview of the experiment in the format of a scientific paper, containing key findings of the experiment and associated metadata. 

% 3.4.6.1 Summary page
\subsubsection{Summary page}
\label{:c:pimp:untargetedpipeline:datainterpretation:summary}

The summary page is designed to attract the user attention to potential findings in his dataset as well as another quality control of the analysis performed. Three sections are presented to the user, each of which having a different purpose.

The first section contains metadata about the experiment provided by the user, a summary of the comparisons performed and a table containing the experiment design. This section also provides information about the method use for processing the data. 

The second section is for quality control purposes. A principal component analysis (PCA) plot showed in Figure~\ref{fig:pca-summary} is provided and allow the assessment of reproducibility between biological replicates and separation or clustering between biological conditions. TIC plots of the samples grouped by biological conditions as shown in Figure~\ref{fig:group-tic} are also provided to assess the reproducibility of the replicates in both positive and negative ionisation mode.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=0.50\textwidth]{30-PiMP/34-untargeted_pipeline/figures/PCA.png}
\par\end{centering}
\caption[Principal Component Analysis visualisation]{\label{fig:pca-summary} Principal Component analysis plot showing the clear separation of the two biological conditions being compared.}
\end{figure}

The third and last section of the summary page provides the user with the most significant quantitatively changing metabolites for each comparison. Those differences are highlighted using histograms and interactive volcano plots as shown in Figure~\ref{fig:volcano}.
Zooming into the volcano plot and accessing to the annotated metabolite corresponding to a particular feature by clicking on dots on the figure allows quick exploration of the most changing compounds in the dataset.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=0.50\textwidth]{30-PiMP/34-untargeted_pipeline/figures/volcano_figure.png}
\par\end{centering}
\caption[Volcano plot visualisation]{\label{fig:volcano} Interactive volcano plot allowing a rapid assessment of the number of peaks that significantly changing between the two conditions. Clicking on a dot allows access to the list of compound annotated by the feature. The visualisation tool also has zooming features.}
\end{figure}

% 3.4.6.2 Raw data
\subsubsection{Raw data}
\label{:c:pimp:untargetedpipeline:datainterpretation:rawdata}

The list of extracted peaks from the raw data, often referred to as the raw data at this stage of analysis is available in the "peaks" tab. The table contains the mass and retention time of the extracted features, the polarity in which it was detected, and the relative abundance value of all the replicates. This page contains no extra statistical or biological data. The purpose of this page is to allow the users to export it in a comma separated format to perform their own statistical analysis using external tools. The evidence side panel provides access to the peak chromatograms and quantitative information as interactive bar plots (Figure~\ref{fig:peak_evidence}).

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=0.75\textwidth]{30-PiMP/34-untargeted_pipeline/figures/peak_evidence_figure.png}
\par\end{centering}
\caption[Evidence panel figures]{\label{fig:peak_evidence} Main figures accessible in the evidence panel of the peak tab, showing a typical dataset derived from a metabolomics experiment comparing biofilm and planktonic \textit{staphylococcus aureus}~\cite{Stipetic2015}. \textbf{a.} Extracted Ion Chromatogram of the peak using the peak set visualisation module. \textbf{b.} Bar plot showing the average intensity and standard deviation of the two conditions and the blank samples. \textbf{c.} Bar plot showing the intensity of the investigated feature in each sample of the planktonic condition.}
\end{figure}

% 3.4.6.3 Statistical analysis
\subsubsection{Statistical analysis}
\label{:c:pimp:untargetedpipeline:datainterpretation:statanalysis}

The results of the statistical analysis performed during the analysis pipeline are presented in two different manners to support two different tasks. The first visualisation module displays a separate table for each performed comparison containing all statistical values available attach to the peaks. The table contains the peak id, the log fold change, the p-value and adjusted p-value, and the log odds. While this table is not straightforward to interpret, it gives access to the user to all statistical values available that are essential for reporting purposes. 

The second module is a unique table summarising the changes for all comparisons performed. For each peak entry represented as a row in the table, the log fold change values for every comparison is given in individual columns (Figure~\ref{fig:comparison_table}). A heat map type visualisation is overlayed on top of the comparison cells when the adjusted p-value is under the 0.05 significance limit. This module allows the user to identify peaks that are significantly changing between biological groups quickly. As no biological information is attached to the table, it allows potential discovery of unknown compounds that would not be reported when interpreted within biological context. However, if potential compounds have been matched to a particular peak, the information is reported in the right panel for the user's information.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{30-PiMP/34-untargeted_pipeline/figures/comparison_table.png}
\par\end{centering}
\caption[PiMP comparison table]{\label{fig:comparison_table} First 4 entries of the summarised comparison table. The first column shows the peak id, the following three column show the log fold change values of the peak in every comparison. The number of column is dependant on the number of comparison performed.}
\end{figure}

% 3.4.6.3 Biological pathways
\subsubsection{Biological pathways}
\label{:c:pimp:untargetedpipeline:datainterpretation:pathways}

The Metabolic maps tab replaces the data in the context of biological pathways. Biological pathways used in this module originate from KEGG. The table displays the list of all KEGG pathways alongside with the number of compounds that form this pathway, the number of annotated and identified compounds found in the dataset that are part of the pathway, and the  of coverage of the pathway by the dataset. A visual version of the coverage is available on the side panel in the form of a pie chart. The side panel also gives access to the pathway visualisation tool which displays the KEGG pathway with contextual data extracted from the dataset.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{30-PiMP/34-untargeted_pipeline/figures/kegg_map.png}
\par\end{centering}
\caption[PiMP pathway visualisation tool]{\label{fig:pathway_visu} Pathway visualisation accessible from the evidence panel of the Metabolic map tab. This example shows the Kegg pathway of Purine metabolism with identified metabolites in yellow and annotated metabolites in grey. A specific comparison was selected (top left of the window) resulting on the display of log fold change indication on the representing the metabolite with a coloured border. The colour scale is indicated at the top of the window.}
\end{figure}

Figure~\ref{fig:pathway_visu} shows the visualisation of a KEGG pathway within the PiMP data environment, identified and annotated metabolite can be identified by gold and silver dot colour respectively. The user can select a specific comparison that was performed during the data analysis to overlay log fold changes data. The log fold changes value are represented by a red and blue border of the dot representing the compound. If several peaks annotate a compound with conflicting log fold changes (one negative and one positive log fold change), the border of the dot is then coloured in purple to inform the user.

% 3.4.6.3 Metabolites tab
\subsubsection{Metabolites}
\label{:c:pimp:untargetedpipeline:datainterpretation:pathways}

The metabolite tab presented in Figure 3.27 brings together all the data such as the statistical values, biological pathways, peak information or compound structure in a unified environment to assist the user in the interpretation of his results. The interface follows the same structure as the other tabs with a main table and an interactive and contextual side panel displaying pieces of evidence for the compound being investigated. 


\begin{sidewaysfigure}
%\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{30-PiMP/34-untargeted_pipeline/figures/metabolite_tab.png}
\par\end{centering}
\label{fig:metabolite_tab}
\caption[PiMP metabolites tab]{Screen shot of the complex organisation of the Metabolite tab with in the center the table, at the top filter, search and export tools, and the evidence panel structured with evidence cards on the right of the window.}
%\end{figure}
\end{sidewaysfigure}

The table contains the minimum information of interest to the user to guide its interpretation. A row in the table correspond to a compound, with the number of columns depending on the number of comparisons performed. The first two columns respectively contain the name of the compound detected and its formula. The following columns contain the log fold change values for all the comparisons, and the last column contains the level of identification of this compound. The identification level can take two values, "identified" if the peak matched by mass and retention time to a standard compound, and "annotated" if the peak matched by mass only to an external compound database. As several peaks can annotate one compound, there can be several possibilities available for the log fold change value. The value displayed is selected according to several criteria: (i) M+H and M-H, the first criterion filters the peaks corresponding mono-isotopic mass calculated from the molecular formula (M) of the compound with an added or subtracted proton (H) depending on the ionisation mode. (ii) When both ions (M+H and M-H) are available, the peak set with the highest intensity value is kept.

The evidence panel is structured into collapsible cards that give contextual information and evidence on the compound being investigated by the user. This information is accessible to the user by simple a click on a row of the metabolite table. Three main cards are displayed by default in the evidence panel, each of which relating to different data. The first card, called compound card, inform on the external or internal (if standards compounds were provided) database in which the compound was found. There can be several databases available with different names given to the same compound. For example, the lactic acid in HMDB is named lactate in KEGG. The structure of the compound is also available when the card is expanded, as seen in Figure~\ref{fig:compound_card}.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=0.30\textwidth]{30-PiMP/34-untargeted_pipeline/figures/compound_card.png}
\par\end{centering}
\caption[PiMP compound card]{\label{fig:compound_card} Compound card showing the structure of the compound detected, changing the database allows to display the database specific name of the compound and its structure.}
\end{figure}

The second card lists all the pathways in which the compound is found, in its collapsed state, the card only gives the number of pathways.

The last card informs on the peaks which annotate the compound under investigation; there are as many cards as there are peaks. This card is designed to give the user the essential information to quickly assess the quality of the peak and the relevance of the match. Each peak card contain the following information: (i) Retention time, (ii) Mass, (iii) Polarity or ionisation mode, (iiii) Type of peak which can be for example a base peak, a related peak or an adduct, (iiiii) The ion information (i.e. M+H, M+Na), (iiiiii) the mass error in ppm. The card also contains the number of other compounds annotated by this peak (excluding the one being investigated) and an identification flag to show that this feature matches the peak of the standard compound. Two visualisation modules are also embedded within the card allowing the visualisation of the peak set and intensity plots as shown in Figure~\ref{fig:peak_card}.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{30-PiMP/34-untargeted_pipeline/figures/peak_card.png}
\par\end{centering}
\caption[PiMP evidence cards organisation]{\label{fig:peak_card} General organisation of the peak card. In the centre is default collapsed card giving the essential information about the peak. On the left, the peak button has been clicked in order to display the EIC of the peak. On the right, the bar plot button has been clicked in order to visualise the average intensity per condition.}
\end{figure}

The last module to support the user in the interpretation of the results is a set of tools to sort, filter and search the data. The sorting tool is directly embedded within the table and allow to sort the results by alphabetical order or high to lowest log fold changes for example. The filtering tool is only based around biological pathways found in KEGG, KEGG organises the pathways into groups forming wider metabolic maps called super-pathways. As shown in Figure~\ref{fig:search_tool}, the user can select a super-pathway which will filter down the pathway selection options to the pathways present in this super-pathway. However, a user can directly select a pathway without narrowing down the selection by choosing a super pathway. The search tool is meant to be used by more experienced users who are looking for something specific. When the user types characters into the search box, the search is launched on the fly on all data available in the table (name, formula, log fold change and identification level). The search box also applies the search on the pathway names. For example, typing "glycolysis" in the search box would apply the same filter as selecting the glycolysis pathway from the pathway filter selection.

\begin{figure}[ht]
\noindent \begin{centering}
\includegraphics[width=0.70\textwidth]{30-PiMP/34-untargeted_pipeline/figures/search_tool.png}
\par\end{centering}
\caption[PiMP metabolites tab search tools]{\label{fig:search_tool} Search tools available at the top of the metabolite tab}
\end{figure}

\clearpage