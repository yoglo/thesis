% 1.3.2 Data acquisition technologies
%\subsection{LCMS metabolomics}
%\label{c:intro:LCMSmetabo}

%\input{"10-introduction/13-lcms_metabolomics/131-lc.tex"}

% 1.2.2.1 Liquid chromatography
%\subsection{Liquid chromatography}
%\label{c:intro:LCMSmetabo:LC}

% 1.2.2.1 Mass spectrometry
\subsection{Measurement and separation technologies}
\label{c:intro:LCMSmetabo:tech}

Mass spectrometry is an analytical technique that separates ionised chemical compounds by their mass-to-charge ratio (\ac{mz})~\cite{Winger1993}. A mass spectrometer is constituted of 3 principal components with different purposes: the ion source imparts a charge to a molecule, the mass analyser separates ions, and the detector records ion signals. Several types of ionisation techniques are available, they are however not all appropriate for LCMS.  Electron ionisation, for example, which produces a high degree of fragmentation is ordinarily coupled to gas chromatography as it cannot be used at atmospheric pressure and require the entire system to be under high vacuum~\cite{Santos2003}. Electrospray ionisation~\cite{yamashita1984electrospray} is the most widely used ion source for LCMS metabolomics and produces soft ionisation (which reduces fragmentation). Alternatively, matrix-assisted laser desorption/ionisation (MALDI)~\cite{Karas1985} is used for imaging, to inform on the spatial distribution profiles of metabolites in tissues~\cite{Lee2010}. Many mass analysers exist with different characteristics; however, modern instruments used in LCMS share high mass resolving power. The mass resolving power is the ability of the mass spectrometer to separate ions with close \textit{m/z} and evaluated using mass accuracy. Mass accuracy is measured in parts per million by calculating the ratio of the \textit{m/z} measurement error to the real \textit{m/z}. Three types of mass analysers are widely used for LCMS: time of flight (\ac{tof}), quadrupoles, and ion traps. ToF analysers create an electric field to accelerate the ions and measure the time ions take to reach the detector. Quadrupole analysers use oscillating electrical fields and a changing potential allowing only ions in a particular range of \textit{m/z} to reach the detector at a given time scanning a wide mass range in a short period. Several types of ion traps exist; three-dimensional quadrupole ion traps, linear quadrupole ion traps and Orbitraps are examples.

Many of the instruments can also perform tandem MS (\ac{msms}). MS/MS is the succession of at least two rounds of mass spectrometry separated by fragmentation. Fragmentation data can inform on the structure of the molecule analysed and is, therefore, a valuable resource in metabolomics to help with the identification of metabolites. Two types of fragmentation can be performed, fragmentation in time and fragmentation in space. Fragmentation in space can be done by using three quadrupoles (Triple Quadrupole) as seen in Figure~\ref{fig:mass_spectrometer}; the first mass analyser isolates an ion, the second analyser acts as a collision cell to fragment the ion, the third analyser isolates a fragment ion. This means a signal will only occur if a characteristic molecular mass is detected, followed by a diagnostic fragment ion~\cite{yost1979triple}. Fragmentation in time is done using one ion trap mass analyser over time such as quadrupole ion trap, and typically involves trapping the ions, selecting an ion of interest by manipulating the electrostatic field in the trap, then collisionally dissociating the analytes using a neutral gas.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{10-introduction/figures/mass_spectrometer.png}
\par\end{centering}
\caption[Triple quadrupole diagram]{\label{fig:mass_spectrometer} Representation of a triple quadrupole performing fragmentation in space. The precursor ion is isolated by the first quadrupole, then fragmented in a collision cell, and the fragments are separated by the third mass analyser. Simpler mass spectrometer only have one mass analyser.}
\end{figure} 


Liquid chromatography adds another dimension to the compound separation (Figure~\ref{fig:LC_system}). In LCMS, this separation is made using High-Performance Liquid Chromatography (\ac{hplc}). The sample to be analysed is injected into the stream of mobile phase and passes through the stationary phase, part of the chromatographic column. Analytes are infused into the mass spectrometer for mass separation as they elute from the column. Diverse columns with different stationary phase properties are used. Hydrophilic interaction liquid chromatography~\cite{Buszewski2012} (\ac{hilic}) columns are used in LCMS metabolomics and separate compounds by increasing polarity. Alternatively, Reversed-phase chromatography methods, which uses a hydrophobic stationary phase is also often used to separate non-polar compounds~\cite{Dettmer2007}.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{10-introduction/figures/LC_system.png}
\par\end{centering}
\caption[Liquid Chromatography - Mass Spectrometry system]{\label{fig:LC_system} Liquid Chromatography - Mass Spectrometry system}
\end{figure} 


% 1.2.2.3 Data format
\subsection{Data\ format}
\label{c:intro:LCMSmetabo:dataformat}

The data produced by mass spectrometers during an LCMS experiment can be very large. The different mass spectrometer manufacturers have developed their own proprietary data format to store and process the data. However, these data formats are not adequate for an academic research environment as they are binary, which make them difficult to read without dedicated software provided by their respective manufacturers. However, several open source data formats have been created over the years in an attempt to provide a unified standard format for MS. Over the past 15 years, two open formats were concurrently developed by the Proteomics Standard Initiative and Seattle Proteome Center, respectively called mzData and mzXML~\cite{Pedrioli2004, Lin2005}.  A joint effort has however emerged since to create a unified open format, mzML~\cite{Martens2011}, which integrates and extend mzData and mzXML data formats. Instrument manufacturers now all provide software libraries to access the data within the binary files and convert it to an open format. This task can be handled by the tool MSConvert~\cite{Chambers2012, Holman2014}, part of ProteoWizard Software.

While some specifications such as metadata information change between the different open formats, the LCMS data itself is stored in a similar manner and can be described as a 3-dimensional chromatogram. Figure~\ref{fig:3d_chromat} illustrates this data by plotting \textit{m/z} versus the retention time in y and x-axes respectively. The z-axis represents the intensity of the signal corresponding to ion counts, the highest signal being set at 100\%. This data represents one polarity only. Two of these data structures are, therefore, present if the instrument is operated in polarity switching mode. Alternatively, positive and negative polarity data can be stored in two separate files.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{10-introduction/figures/3d_chromat.png}
\par\end{centering}
\caption[LCMS 3-dimensional chromatogram]{\label{fig:3d_chromat} Representation of a 3-dimensional chromatogram produced by LCMS data acquisition.}
\end{figure} 

This complex data structure allows to approaching the data in two different manners. Figure~\ref{fig:3d_chromat_mass_spec_section} illustrates a mass spectrum and the information it contains in the context of LCMS data structure. For each of the time points there is a corresponding mass spectrum containing MS peaks. Those peaks in a mass spectrum represent the molecules that eluted from the column at a specific retention time. Mass spectra become more complex as the number of compounds eluting from the column at the same retention time.

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{10-introduction/figures/3d_chromat_mass_spec_section.png}
\par\end{centering}
\caption[Single mass spectrum selected from an LCMS data file]{\label{fig:3d_chromat_mass_spec_section} Single mass spectrum selected from an LCMS data file.}
\end{figure}

The data can also be approached in a transversal manner from mass spectra in order to look at a single ion (\textit{m/z}) over time. Chromatographic peaks observed in extracted ion chromatograms as shown in Figure~\ref{fig:3d_chromat_mass_spec_section2} show the elution of a single ion through the chromatographic column. It can, therefore, show the separation of two species of the same mass but different affinity with the column. 

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{10-introduction/figures/3d_chromat_rt_section.png}
\par\end{centering}
\caption[LCMS 3-dimensional chromatogram - Extracted ion chromatogram]{\label{fig:3d_chromat_mass_spec_section2} Extracted ion chromatogram of a LCMS file.}
\end{figure}

% 1.2.2.1 Liquid chromatography

\subsection{Data processing}
\label{c:intro:LCMSmetabo:dataprocessing}

Many tools have been developed to support LCMS data processing, while they do not always provide the same features, a common data processing pipeline is conserved across those different tools. The user interface can, however, vary from command lines to a dedicated graphical user interface for stand-alone tools. In the recent years, web-based data processing pipelines have also emerged, providing a graphical user interface through web browsers such as Galaxy based pipelines~\cite{Afgan2016} and overcoming any installation requirements. This section below describes the different steps of LCMS data processing although some tools can provide some variations of this general pipeline.       

\subsubsection{Peak detection}
\label{c:intro:LCMSmetabo:dataprocessing:peakdetection}

The peak detection is applied to LCMS data as it reduces considerably the size of the data to handle. During this step, the data structure previously presented is converted to a list of peaks, each entry being characterised by its \textit{m/z}, retention time and intensity. Peak detection is complex, and many tools tend to use the same algorithm to perform this task. CentWave~\cite{Tautenhahn2008}, the most widely used peak detection algorithm is implemented as part of XCMS~\cite{Smith:2006} and is based on centroid mode spectra. Other peak detection algorithms are available such as MetAlign~\cite{Tikunov2005} or CentroidPicker in MZmine~\cite{Katajamaa:2006}, but cannot be used by third party tools as they are part of stand-alone software. These algorithms treat each sample spectrum independently and rely on manually defined parameters that have a significant impact on the quality of the peak detection. Some of these parameters are in direct relation to specifications of the instrument used for the data acquisition such as the mass deviation parameter in CentWave which requires knowing the mass accuracy of the instrument. These algorithms are therefore aimed to be used by experienced users for optimal results.

\subsubsection{Peak alignment}
\label{c:intro:LCMSmetabo:dataprocessing:peakalignment}

The vast majority of metabolomics experiments are based on the assumption that differences will be observed between two different experimental group of samples, defined by either biological or technical replicates. This assumption implies that metabolite levels are comparable within and across experimental groups. As the peak detection is performed  independently for each replicate sample (LCMS run), matching peaks across LCMS runs corresponding to the same molecular ion is essential for the downstream analysis. However, analytical platforms used in LCMS can produce data with large, non-linear retention time drift between LCMS runs. The peak alignment step addresses this issue and has been implemented using different methods. It produces as an output a list of \textit{peaksets} containing the aligned peaks from each LCMS run.  

Different warping-based alignment methods are widely used for LCMS data processing. These methods attempt to model the retention time drift between runs to correct it. Two main types of warping based alignment have been implemented and are based either on the total ion current (\ac{tic}) or the extracted peaks themselves. TIC-based algorithms such as Dynamic Time Warping (\ac{dtw})~\cite{Kassidas1998}, Parametric Time Warping (\ac{ptw})~\cite{Eilers2004, VanNederkassel2006}, Correlation Optimal Warping (\ac{cow})~\cite{Nielsen1998} and Continuous Profile Mode (\ac{cpm})~\cite{NIPS2004_2721} are not used by modern software as they take a reductive approach by using TICs only and ignoring the complex information of LCMS data. These methods were found to be inadequate for LCMS data as they often fail to align overlapping peaks (co-eluting compounds). An improvement of the COW methods which combine it to a component detection algorithm (\ac{coda}) was however developed and showed a higher alignment quality~\cite{Christin2008}. The same type of approach was taken to improve PTW and DTW algorithms for LCMS data by combining it with CODA~\cite{Christin2010}. An extension of DTW termed Ordered Bijective Interpolated Warping (\ac{obiwarp})~\cite{And2006}, available in XCMS, shows improved alignment results and is now commonly used. 

Alternative alignment methods can also be used such as Direct Matching which compares peaks across LCMS runs based on similarities without warping. Many implementations of this method have been proposed using different similarities measures~\cite{Voss2011, Hoffmann2012, Ballardini2011} and are available in various LCMS data analysis tools such as Join Aligner in MZmine~\cite{Katajamaa:2006}.

Finally, a simpler labelled LCMS data alignment can also be used; it, however, requires the injection of internal standards in the experimental samples, which increases the complexity of sample preparation. 

While several algorithms can suit the task of LCMS peak alignment, it is hard to assess what algorithm provides the best solution. This is in great part due to the lack of comparative evaluation at the time of publication as outlined by R. Smith, et al.~\cite{Smith2013}. The choice of an alignment algorithm can, however, be made by using recent comparative reviews~\cite{Lange2008, Smith2015}.

\subsubsection{Data filtering}
\label{c:intro:LCMSmetabo:dataprocessing:filtering}

Many data filters can be used to remove undesired signal~\cite{Berg2013}. For example, Reproducibility Standard Deviation filter~\cite{Scheltema2008} available in mzMatch~\cite{Scheltema:2011} helps to eliminate signal that is too variable between replicates. More common filters are available in the different data processing tools such as blank filter, noise filter or a minimum number of detection. The blank filter discards any peak that is higher in the blank samples (generally extraction solvent) than the experimental samples as they can be considered as contaminants. The minimum detection number allow discarding peaks that are present in a limited number of samples, which often correspond to noise signals.

\subsubsection{Gap filling}
\label{c:intro:LCMSmetabo:dataprocessing:gapfilling}

In some cases, peaks can be missing from a peakset due to a misalignment or rejection during peak detection because of a poor shape or high background signal. The gap filling step aims to recover this missing signal directly from the raw files. This step gives better insurance on the true absence of a peak.

\subsubsection{Peak grouping}
\label{c:intro:LCMSmetabo:dataprocessing:peakgrouping}

Undesired in-source fragmentation often happens during the ionisation process which results in the production of multiple peaks per metabolite. Similarly, the sample preparation can cause the formation of adducts formed by the adduction of an ionic species such as different salts to a molecule. Beyond the ion suppression resulting from these formations~\cite{Annesley2003}, it also results on the production of multiple peaks for a single molecule. Finally, naturally occurring isotopes such as \textsuperscript{13}C can produce several peaks that follow the isotopic distribution of the element. The signal generated by these products of the precursor ion caused by those different mechanisms are commonly called related peaks. 

The peak grouping step attempts to identify these related peaks and group them together with the precursor ion. Different methods often based on known chemical relationships can be used to create these peak groups. For example, mzMatch uses a clustering method based on intensity and peak shapes while CAMERA~\cite{Kuhl2012} groups related peaks using multiple integrated methods reconstructing a similarity graph.

This grouping step can be applied to the data at different stages of the pipeline but results in a consistent reduction in the number of relevant peaks which facilitate the peak identification stage. 

\subsubsection{Peak identification}
\label{c:intro:LCMSmetabo:dataprocessing:peakid}

Peak identification is crucial in order give a biological meaning the data generated. This process attempts to match peaks from a given LC-MC dataset to molecular formulas and compound identities. It is however not a trivial task due to a high number of possible associations between a peak and metabolites. The Chemical Analysis Working Group as part of the Metabolomics Standard Initiative (\ac{msi}) created a 4 level scheme to help to report metabolite identification and annotation in a uniformed manner between studies~\cite{Sumner2007}. The first level, considered as highest ranked identification, necessitate a match of a minimum of two independent and orthogonal data relative to an authentic compound analysed under identical experimental conditions such retention time and accurate mass. Authentic compounds data is acquired by running authentic standards mix on the instrument. The second level of identification is based upon spectral similarity with a public spectral library. The level 3 corresponds to putatively characterised compound classes, and the level 4 designate unknown compounds. 

In standard untargeted approaches, a finite set of authentic standard compounds is run, which limits the number of peaks that can be annotated as level 1 identification. The majority of the other peaks are identified using accurate mass from public databases, KEGG~\cite{Kanehisa2014}, PubChem~\cite{Kim2016}, HMDB~\cite{Wishart2013} and LIPID MAPS~\cite{Sud2007} are some examples amongst many available. However, mass accuracy is often not enough for unambiguous identification~\cite{Kind2006}.

While \textit{in silico} retention time prediction can help with the identification process~\cite{Creek2011}, the most promising avenue for addressing this issue is the use of fragmentation data acquired by tandem MS (MS/MS). Fragmentation data can indeed offer structural information about compounds and therefore provide better support for peak identification. The same approach can be used for the identification process, matching fragmentation spectra against publicly available libraries. Many libraries are available with different degree of curation, matching options, and a varying number of spectra. MassBank~\cite{Horai2010} and ChemSpider~\cite{Pence2010} figure among the most widely used spectral databases.

Peak identification process is improving every day as spectral libraries cover an increasing number of compounds, it remains, however, one of the biggest challenges the metabolomics community has to overcome to lead to a better data interpretation in biological context.

\subsubsection{Statistical analysis}
\label{c:intro:LCMSmetabo:dataprocessing:stat}

A normalisation step is often required proceeding to the differential analysis of the dataset. The complexity of this task is highly dependent upon the size and the property of the dataset. Over a certain number of samples analysed, the data collection needs to be performed in separate batches before being merged into one large dataset. This procedure results in biased dataset values due to the variation of LCMS platforms over time. Solutions proposed are still in their infancy although the problem has been addressed many times over the past few years~\cite{FernAndez-Albert2014, Wehrens2016}. Several methods for single batch data normalisation use different approaches that can be divided into two main approaches~\cite{Ejigu2013}. Methods-driven approaches use internal standard material references to base the normalisation upon. The standard used as reference rarely cover all metabolite classes present in the samples which limit the normalisation efficiency. This method is also not cost effective as the stable isotopes used as internal standards are expensive. Data-driven approaches are the most widely used normalisation methods and are based on the assumption that most metabolites produce a constant signal across samples, these methods have the benefit not to require to know the identity of the metabolites.

Once normalised, statistical analysis such as ANOVA, t-test, false discovery rate~\cite{benjamini1995controlling} and principal component analysis can be performed.

\subsection{Data analysis platforms}
\label{c:intro:LCMSmetabo:platforms}

Data generated by LCMS experiments is very complex, and its visualisation is essential at many steps of the analysis pipeline. Visualisation of the overall signal or raw files produced by the instruments is well supported by proprietary software provided by manufacturers. This software also provides search and curation tools to explore the data and revealed to be technical. They are therefore aimed at trained users which are expert in the field. Similarly, some software also offers visualisation tools for open source raw data formats~\cite{May2009, Katajamaa:2006}. Most software, however, provides visualisation tools corresponding to the specific analysis tasks they support. For instance, mzMatch supports the visualisation of extracted peaks with PeakML Viewer, and XCMS allow the visualisation of peaks before and after alignment. 

There is, however, no standard when it comes to visualisation of data analysis results of metabolomics experiments. The typical representation of the data being a matrix where each row corresponds to a metabolite or an unannotated peak and each column a biological sample, and each matrix entry the intensity value of a metabolite in a sample. This very crude data representation is very limiting for the interpretation and omits major biological and statistical related information. Many attempts have been made to organise the results in a coherent manner to highlight the different type of information connected to the metabolites. These efforts are specifically made in end-to-end data analysis software which integrates all processing steps. IDEOM~\cite{Creek:2012} proposes an organisation into tabs in an excel spreadsheet (Figure~\ref{fig:ideom_comparison_tab}), with one of them summarising the metabolites found in the dataset along with t-test p-values and biological pathway information. 

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{10-introduction/figures/ideom_comparison_tab.png}
\par\end{centering}
\caption[Ideom result page]{\label{fig:ideom_comparison_tab} Comparison tab showing the results of LCMS data analysis in IDEOM.}
\end{figure} 

%While this type of representation can be useful, no access to raw peaks is given which implies that they have to be visualise using a third party software.

Other standalone applications propose similar approaches such as MAVEN~\cite{Clasquin:2012} which give information about the biological compound under investigation in its pathway view.

More recently, web-based software has been developed which enable an easy access with no installation requirements for the user. This very accessible software has attracted considerable interest from the biological research community and helps disseminate and systematise the use of metabolomics in biological science. Some of these programs support the entire data analysis such as XCMS Online~\cite{Tautenhahn2012} or Workflow4Metabolomics~\cite{Giacomoni:2015}, others tend to focus particularly on a particular task. MetaboAnalyst~\cite{Xia:2012} for example, offer extensive statistical tools for metabolomics data. XCMS Online is the first end-to-end data analysis software which attempts to allow non-experts to perform their own data analysis~\cite{Gowda2014}. It was in part achieved by introducing simplified parametrisation and interactive visualisation (Figure~\ref{fig:XCMS_cloud_plot}). Other software such as OpenMS~\cite{Rost2016} which was first developed for proteomics provides now support for metabolomics data analysis. 

\begin{figure}[ht!]
\noindent \begin{centering}
\includegraphics[width=1.0\textwidth]{10-introduction/figures/XCMS_cloud_plot.png}
\par\end{centering}
\caption[XCMS online interactive cloud plot]{\label{fig:XCMS_cloud_plot} Innovative interactive visualisation tool available in XCMS Online.}
\end{figure}  

\subsubsection{Shortcoming of current data analysis platforms}
\label{c:intro:LCMSmetabo:platforms:short}

This section presents the shortcoming of the current software attempting to support end-to-end LCMS metabolomics data analysis, from raw data to biological interpretation.

The first common limitation of the tools presented in the previous section is the level of understanding required for the user to perform an analysis. Indeed, many settings needing an in-depth knowledge of LCMS technology has to be manually entered by the user. Mass and retention time window for feature detection, alignment parameters, are some of many examples. This requirement currently limits the usage of these tools to mature audience forcing inexperienced users such as biologists or clinicians to outsource their metabolomics analyses to bioinformaticians.

The second limitation shared by most tools is the static and fragmented structure of the applications. While modular designs can be useful to expand the feature set that software has to offer responsively and can provide the user with many analysis options, the lack of connectivity between these modules results in a fragmented overall architecture. Two major problems arise from this approach. First, tools such as MetaboAnalyst present the data analysis pipeline in the form of functional modules that the user has to choose from, which implies some level of understanding from the user in order to run modules in a coherent, sequential manner. Other tools such as IDEOM present the workflow as an integrated pipeline. However, user intervention is still required at each step of the pipeline which limits the turn-around time for a complete analysis of the data considerably. Finally, Galaxy based software also necessitates basic understanding on how to organise a data analysis pipeline.

The same static approach is often taken with regards to data visualisation. While most software provide features to generate figures such as Principal component analysis (\ac{pca}) or volcano plots which can be interpreted on their own without surrounding information, the same method is often taken to present extracted-ion chromatograms (\ac{eic}s) or mass spectra. This approach of generating static pictures to display specific information isolates the data from the general context of the analysis which makes it harder to interpret. XCMS Online started addressing these issues by organising the analysis pipelines into jobs and creating dynamic and interactive visualisation tools which can help users in better understanding their data.

Currently, available tools can be divided into two main groups, stand-alone software and web-based applications. Stand-alone applications necessitate the local installation of the software and its library dependencies on personal computers. This task is often difficult for users with no fundamental skills in computer science. Collaborative work within such environments can also become a challenge as it requires every party involved to have access to the same version of the same tool. Moreover, sharing large metabolomics datasets is not a trivial task due to the size of the raw and processed data. Web-based applications do not suffer from these limitations as they usually offer sharing features and direct access to the data through a web browser. 

One of the key components to enable users to extract meaningful biological insight from metabolomics datasets is the biological context under which the results are investigated. While the fragmented structure previously discussed substantially limits this interpretation  process, some tools are beginning to integrate pathway enrichment and analysis tools. However, many still require the use of third-party software to replace metabolomics data into a larger biological context. Manual export, formatting, and import of the data is required whether these applications are web-based~\cite{Xia2010, Yamada2011, Leader2011, Cottret2010}, stand-alone~\cite{Kutmon2015} or Cytoscape plugins~\cite{Jourdan2008, Karnovsky2012}, which creates yet another barrier for inexperienced users to interpret their data entirely.

%\subsection{Data interpretation}
%\label{c:intro:LCMSmetabo:dataprocessing}

